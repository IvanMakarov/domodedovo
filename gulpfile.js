const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCss = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const htmlmin = require('gulp-htmlmin');
const browsersync = require('browser-sync');

gulp.task('js', () => {
    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./js'))
        .pipe(browsersync.stream())
});

gulp.task('css', () => {
    return gulp.src('./src/sass/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(autoprefixer())
        .pipe(cleanCss())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css'))
        .pipe(browsersync.stream())
});

gulp.task('html', () => {
    return gulp.src('./src/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('./'))
        .pipe(browsersync.stream())

});

gulp.task('php', () => {
    return gulp.src('./src/**/*.php')
    .pipe(gulp.dest('./'))
    .pipe(browsersync.stream())
})

gulp.task('sync', () => {
    browsersync.init({
        proxy: 'domodedovo3',
        notify: false
    })
})

gulp.task('watch', () => {
    gulp.watch(['./src/sass/*.scss'], ['css']);
    gulp.watch(['./src/js/**/*.js'], ['js']);
    gulp.watch(['./src/**/*.html'], ['html']);
    gulp.watch(['./src/**/*.php'], ['php']);
    gulp.watch(['./**/*.php',
                './**/*.html',
                './**/*.css',
                './**/*/js'
                ]).on('change', browsersync.reload);
});



gulp.task('default', ['sync', 'watch']);
