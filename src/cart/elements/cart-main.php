<div class="content">
    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
    <main class="main">
        <section class="cart">
            <h1 class="section--heading">Корзина</h1>
            <section class="cart--content">
                <ul>
                    <li class="cart--item-wrapper">
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/cart/elements/cart-item.html') ?>
                    </li>
                    <li class="cart--item-wrapper">
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/cart/elements/cart-item.html') ?>
                    </li>
                    <li class="cart--item-wrapper">
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/cart/elements/cart-item.html') ?>
                    </li>
                    <li class="cart--item-wrapper">
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/cart/elements/cart-item.html') ?>
                    </li>
                </ul>
            </section>
            <section class="cart--controls">
                <div class="content--row__flex cart--total-container">
                    <section class="cart--total">
                        <p>Итого: <span class="cart--total-value js-cart-total" data-value="18800">18800</span>&nbsp;<span class="cart--total-unit">р.</span></p>
                    </section>
                </div>
                <div class="content--row__flex cart--controls">
                    <button class="cart--reset"><span class="cross">&times;</span> Очистить корзину</button>
                    <a href="/order" class="btn cart--order order">Оформить заказ</a>
                </div>
            </section>
        </section>
    </main>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
</div>