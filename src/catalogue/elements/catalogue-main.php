<main class="main">
    <div class="content">
        <div class="columns">
            <div class="content--section">
                <section class="intro catalogue--intro" style="background-image: url('/img/intro-catalogue.jpg')">
                    <p class="intro--text">&laquo;КАЧЕСТВО&nbsp;&mdash; ЭТО КОГДА ВСЕ ДЕЛАЕШЬ ПРАВИЛЬНО,<br>ДАЖЕ КОГДА НИКТО НЕ&nbsp;СМОТРИТ&raquo;</p>
                </section>
            </div>
        </div>

        <section class="catalogue--main">
            <h2 class="section--heading">Каталог</h2>
            <ul class="catalogue--items columns">
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
                </li>
            </ul>
        </section>

        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
    </div>
</main>
