<div class="content-wrapper">
    <div class="content">
        <div class="breadcrumbs--wrapper">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        </div>   
        <div class="content--row__flex columns">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/catalogue/elements/sidebar.html') ?>
            <main class="main">
                <section class="item--description">
                    <?php
                        $headerLevel = "h1";
                        include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php');
                    ?>
                </section>
                <section class="item--details">
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/catalogue/elements/item-details.html') ?>
                </section>
            </main>
        </div>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
    </div>
</div>