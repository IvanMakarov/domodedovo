<div class="content-wrapper">
    <div class="content">
        <div class="breadcrumbs--wrapper">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        </div>     
        <div class="content--row__flex columns">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/catalogue/elements/sidebar.html') ?>
            <main class="main">
                <section class="category--description">
                    <h1 class="category--heading">
                        Сваи фундаментные
                    </h1>
                    <p class="category--description-text">
                        Распространенные ленточные фундаменты строятся только на&nbsp;устойчивых грунтах и&nbsp;не&nbsp;подходят для некоторых регионов страны. Если вы&nbsp;планируете, строительство частного дома на&nbsp;почве с&nbsp;неблагоприятными условиями, компания &laquo;СтройПартнер&raquo; предлагает купить железобетонные забивные сваи по&nbsp;выгодным ценам. Они формируют надежную опору на&nbsp;песчаных, глинистых, суглинистых почвах, грунтах с&nbsp;большим содержанием воды или на&nbsp;участках с&nbsp;высоким уровнем грунтовых вод, а&nbsp;также в&nbsp;районах вечной мерзлоты.
                    </p>
                    <p class="category--description-text">
                        Распространенные ленточные фундаменты строятся только на&nbsp;устойчивых грунтах и&nbsp;не&nbsp;подходят для некоторых регионов страны. Если вы&nbsp;планируете, строительство частного дома на&nbsp;почве с&nbsp;неблагоприятными условиями, компания &laquo;СтройПартнер&raquo; предлагает купить железобетонные забивные сваи по&nbsp;выгодным ценам. Они формируют надежную опору на&nbsp;песчаных, глинистых, суглинистых почвах, грунтах с&nbsp;большим содержанием воды или на&nbsp;участках с&nbsp;высоким уровнем грунтовых вод, а&nbsp;также в&nbsp;районах вечной мерзлоты.
                    </p>
                </section>
                <ul class="category--items">
                    <li>
                        <?php
                            $headerLevel = "h2";
                            include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php');
                        ?>            
                    </li>
                    <li>
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php'); ?>
                    </li>
                    <li>
                        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php'); ?>
                    </li>
                </ul>
            </main>
        </div>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
    </div>
</div>
