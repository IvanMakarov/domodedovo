<div class="content">
    <main class="main">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        <h1 class="section--heading">Контакты</h1>
        <section class="contacts content--row__flex columns">
            <div class="contacts--column">
                <section class="contacts--main">
                    <ul class="contacts--list">
                        <li class="contacts--list-item address">142000, Московская область, г.&nbsp;Домодедово, владение «Завод железобетонных изделий», офис&nbsp;19</li>
                        <li class="contacts--list-item phone"><a href="tel:+79164744613">+7 916 474-46-13</a></li>
                        <li class="contacts--list-item email"><a href="mailto:dmu3@inbox.ru">dmu3@inbox.ru</a></li>
                        <li class="contacts--list-item hours">Режим работы с&nbsp;09:00 до&nbsp;19:00</li>
                    </ul>
                </section>
                <section class="legal-details">
                    <h2 class="section--heading">
                        Реквизиты
                    </h2>
                    <ul class="legal-details--list">
                        <li class="legal-details--list-item">
                            <h3>Полное наименование организации в&nbsp;соответствии с&nbsp;учредительными документами: </h3>
                            <p>
                                Общество с&nbsp;ограниченной ответственностью &laquo;Домодедовское Монтажное управление 3&raquo;
                            </p>
                        </li>
                        <li class="legal-details--list-item">
                            <h3>Сокращенное наименование организации: </h3>
                            <p>ООО &laquo;Домодедовское Му-З&raquo;</p>
                        </li>
                        <li class="legal-details--list-item">
                            <h3>Юридический адрес:</h3>
                            <p>
                                142000, Московская обл., г.&nbsp;Домодедово, Центральный мкр, Промышленная&nbsp;ул, дом &#8470;&nbsp;9, оф.19
                            </p>
                        </li>
                        <li class="legal-details--list-item">
                            <h3>Фактический адрес:</h3>
                            <p>           
                                142000, Московская обл., г.&nbsp;Домодедово, Центральный мкр, владение &laquo;Завод железобетонных изделий&raquo;, Промышленная&nbsp;ул, дом &#8470;&nbsp;9, оф.19
                            </p>
                        </li>
                        <li class="legal-details--list-item">
                            <table>
                                <tr>
                                    <th>ИНН/КПП	</th>
                                    <td>5009084614/500901001</td>
                                </tr>
                                <tr>
                                    <th>ОГРН</th>
                                    <td>1125009003635</td>
                                </tr>
                                <tr>
                                    <th>ОКПО</th>
                                    <td>13275164</td>
                                </tr>
                                <tr>
                                    <th>ОКВЭД</th>
                                    <td>45.21</td>
                                </tr>
                                <tr>
                                    <th>ОКАТО</th>
                                    <td>46209000000</td>
                                </tr>
                                <tr>
                                    <th>ОКТМО</th>
                                    <td>46709000</td>
                                </tr>                            		     		
                            </table>
                        </li>
                        <li class="legal-details--list-item">
                            <h3>Банковские реквизиты:</h3>
                            <table>
                                <tr>
                                    <th>р/счет</th>
                                    <td>40702810801910000133 в&nbsp;ОАО &laquo;БАНК МОСКВЫ&raquo; г.&nbsp;Москва</td>
                                </tr>
                                <tr>
                                    <th>корсчет</th>
                                    <td>30101810500000000219</td>
                                </tr>
                                <tr>
                                    <th>БИК</th>
                                    <td>044525219</td>
                                </tr>
                            </table>
                        </li>
                        <li class="legal-details--list-item">
                            <h3>Главный бухгалтер:</h3>
                            <p>Кузьминич Анатолий Александрович</p>
                            <p>Тел. <a href="tel:+79164744613">+7 916 474-46-13</a></p>
                            <p>Email: <a href="mailto:">dmu3@inbox.ru</a></p>
                        </li>   
                    </ul>
                </section>
            </div>
            <div class="contacts--column">
                <section class="map-container">
                    <div class="map" id="map"></div>
                    <p>
                        С&nbsp;Cадового кольца поворачиваете в&nbsp;сторону Калужской&nbsp;пл., далее двигаетесь прямо до&nbsp;поворота на&nbsp;ул. Стасовой, примерно через 450 метров поверните направо на&nbsp;3-й Донской пр-д.
                    </p>
                </section>
                <form class="form feedback" action="">
                    <fieldset>
                        <h2 class="form--heading">Обратная связь</h2>
                        <input class="form--field" type="text" name="feedback-region" placeholder="Регион / Район *" required>
                        <input class="form--field" type="tel" name="feedback-tel" placeholder="Телефон *" required>
                        <input class="form--field" type="email" name="feedback-email" placeholder="E-mail *" required>
                        <textarea class="form--field" name="feedback-extra" id="" cols="30" rows="10" placeholder="Дополнительная информация"></textarea>        
                    </fieldset>
                    <div class="content--row__flex form--submit">
                        <button class="btn submit" type="submit">
                            Отправить
                        </button>
                    </div>
                </form>
            </div>
        </section>   
    </main>
</div>


		
		
		








      




 