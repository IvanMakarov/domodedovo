<article class="catalogue-item" data-id="pile-sc-4-30">
    <div class="content--row__flex columns">
        <section class="catalogue-item--img">
            <div class="catalogue-item--img-wrapper">
                <img src="/img/catalogue/pile.jpg" alt="">
            </div>
        </section>
        <section class="catalogue-item--details">
            <div class="content--row__flex">
                <<?= $headerLevel?> class="catalogue-item--heading">
                    <a href="/catalogue/subcatalogue/item">Свая железобетонная с&nbsp;центральным армированием Сц&nbsp;4-30</a>
                </<?= $headerLevel?>>
                <section class="catalogue-item--price">
                    <span class="catalogue-item--price-value js-price-value" data-id="pile-sc-4-30" data-value="3800.17">3800.17</span>&nbsp;р./шт
                </section>
            </div>
            <p class="catalogue-item--description">
                Сваи предназначены для применения во&nbsp;всех климатических районах, для устройства свайных фундаментов зданий и&nbsp;сооружений.
            </p>
            <div class="content--row__flex">
                <table class="catalogue-item--chars">
                    <tr>
                        <td>Длинна, мм</td>
                        <td>5000</td>
                    </tr>
                    <tr>
                        <td>Ширина, мм</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <td>Высота, мм</td>
                        <td>170</td>
                    </tr>
                    <tr>
                        <td>Вес, кг</td>
                        <td>1000</td>
                    </tr>
                </table>

                <section class="catalogue-item--total">
                    <p>Итого: <span class="catalogue-item--total-value js-price-total" data-id="pile-sc-4-30" data-value="7600.34">7600.34</span>&nbsp;<span class="catalogue-item--price-unit">р.</span></p>
                </section>

                <form action="" class="catalogue-item--order">
                    <section class="content--row__flex">
                        <button class="btn remove js-counter-control" data-id="pile-sc-4-30">&minus;</button>
                        <input class="order-number js-count" type="number" value="1" min="1" data-id="pile-sc-4-30">
                        <button class="btn add js-counter-control" data-id="pile-sc-4-30">+</button>
                    </section>
                    <button class="btn order js-tocart" data-id="pile-sc-4-30">Заказать</button>
                </form>
            </div>
        </section>
    </div>
</article>
