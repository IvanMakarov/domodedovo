<div class="columns">
    <section class="catalogue content--section">
        <h2 class="section--heading">Каталог</h2>
        <div class="slider-wrapper">
            <section class="slider owl-carousel owl-theme main-catalogue-slider">
                <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item-preview.html') ?>
            </section>
            <div class="slider-pages main-catalogue-slider-pages"></div>
            <div class="slider-nav main-catalogue-slider-nav"></div>
        </div>
    </section>
</div>
