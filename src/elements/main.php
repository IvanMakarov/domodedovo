<main class="main">
    <div class="content">

        <div class="columns">
            <div class="content--section">
                <section class="intro" style="background-image: url('/img/intro.png')">
                    <p class="intro--text">Лучшее качество<br>по лучшим ценам</p>
                </section>
            </div>
        </div>

        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-slider.php') ?>

        <div class="content--row__flex columns">
            <section class="about content--section column">
                <h2 class="section--heading">О компании</h2>
                <section class="about--text content--container">
                    <article>
                        <p>
                            Взяв курс на&nbsp;профессиональное развитие своих сотрудников и&nbsp;клиенто-ориентированный сервис, Компания непрерывно развивается и&nbsp;увеличивает свою долю на&nbsp;рынке из&nbsp;года в&nbsp;год.
                        </p>
                        <p>
                            Сегодня мы&nbsp;являемся официальным дистрибьютором десятков заводов-производителей строительных материалов от&nbsp;газобетона и&nbsp;кирпича до&nbsp;отделочных материалов и&nbsp;кровли, что делает нас не&nbsp;только и&nbsp;увеличивает свою долю на&nbsp;рынке из&nbsp;года в&nbsp;год.
                        </p>
                        <blockquote class="bq">
                            <span class="bq--first">«О качестве помнят, даже</span><span class="bq--second">когда цена уже давно забыта»</span>
                        </blockquote>
                    </article>
                </section>
            </section>
            <section class="news content--section column">
                <h2 class="section--heading">Новости</h2>
                <section class="news--text content--container">
                    <article class="news--article">
                        <section class="news--date">
                            11.09.2017
                        </section>
                        <section class="news--text">
                            <p>
                                Новые кирпичи от&nbsp;Голицынского завода!
                            </p>
                            <p>
                                Новые товары....
                            </p>
                            <a href="">Читать</a>
                        </section>
                    </article>
                    <article class="news--article">
                        <section class="news--date">
                            11.09.2017
                        </section>
                        <section class="news--text">
                            <p>
                                Новые кирпичи от&nbsp;Голицынского завода!
                            </p>
                            <p>
                                Новые товары....
                            </p>
                            <a href="">Читать</a>
                        </section>
                    </article>
                </section>
            </section>
        </div>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
    </div>
</main>
