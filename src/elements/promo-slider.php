<div class="columns">
    <section class="promo content--section">
        <h2 class="section--heading">Акции</h2>
        <div class="slider-wrapper">
            <section class="slider owl-carousel owl-theme promo-slider">
                <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?>
            </section>
            <div class="slider-pages promo-slider-pages"></div>
            <div class="slider-nav promo-slider-nav"></div>
        </div>
    </section>
</div>
