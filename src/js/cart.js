function cart() {
    const controls = [...document.querySelectorAll('.js-counter-control')];
    const inputEvent = new Event('input', {
        'bubbles': true,
        'cancelable': true
    });
    
    if (controls) {
        controls.forEach(bindUpdateCounter)
    }
    
    function bindUpdateCounter(btn) {
        btn.addEventListener('click', updateCounter);
    }
    
    function updateCounter(e) {
        e.preventDefault();
        
        const counter = document.querySelector(`.js-count[data-id=${e.target.dataset.id}]`);   
        counter.addEventListener('input', preventNegatives);
    
        counter.addEventListener('input', updateItemTotal);
    
        counter.addEventListener('input', updateCartTotal);
        
        if (e.target.classList.contains('add')) {
            ++counter.value;
            counter.dispatchEvent(inputEvent)
        }
        if (e.target.classList.contains('remove')) {
            --counter.value;
            counter.dispatchEvent(inputEvent)
        }
    }
    
    function preventNegatives(e) {
        if (e.target.value < 0) {
            e.target.value = 0
        }
    }
    
    function updateItemTotal(e) {
        const price = document.querySelector(`.js-price-value[data-id=${e.target.dataset.id}]`);
    
        const total = document.querySelector(`.js-price-total[data-id=${e.target.dataset.id}]`);
    
        if (total) {
            total.textContent = total.dataset.value = (Number(price.dataset.value) * e.target.value).toFixed(2)
        }
    }
    
    function updateCartTotal(e) {
        const totals = [...document.querySelectorAll('.js-price-total')];
        const cartTotal = document.querySelector('.js-cart-total');
        if (cartTotal) {
            cartTotal.textContent = cartTotal.dataset.value = totals.reduce(toCartTotal, 0).toFixed(2)
        }
    }
    
    function toCartTotal(total, item) {
        total += Number(item.dataset.value);
        return total
    }
}

document.addEventListener('DOMContentLoaded', cart);