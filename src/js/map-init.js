ymaps.ready(init);
let myMap,
    myPlacemark;

function init(){     
    myMap = new ymaps.Map("map", {
        center: [55.446577, 37.7700983],
        zoom: 11,
        controls: []
    });

    myPlacemark = new ymaps.Placemark([55.446577, 37.7700983], {
    });
    
    myMap.geoObjects.add(myPlacemark);
}