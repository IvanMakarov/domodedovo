function modal() {
    const orderCall = document.querySelector('.js-order-call');
    const modalCont = document.querySelector('.js-modal-container');
    const thankyou = modalCont.querySelector('.thank-you');
    const callForm = modalCont.querySelector('.order-call');
    const closeModals = [...document.querySelectorAll('.js-close-modal')];
    const body = document.querySelector('.body');

    if (orderCall) {
        orderCall.addEventListener('click', (e) => {
            show(modalCont);
            preventScroll(body);
            show(callForm);
        })
    }

    if (modalCont) {
        modalCont.addEventListener('click', (e) => {
            if (e.target === modalCont) {
                hide(modalCont)
                allowScroll(body);
            }
        })
    }

    closeModals.forEach(btn => {
        btn.addEventListener('click', (e) => {
            e.preventDefault();
            hide(document.querySelector(`.modal[data-id="${btn.dataset.id}"]`));
            hide(modalCont);
            allowScroll(body);
        })
    })
    
    if (callForm) {
        const form = callForm.querySelector('form');
        form.addEventListener('submit', (e) => {
            e.preventDefault();
            hide(callForm);
            show(thankyou);
        })
    }

    function show(item) {
        item.classList.remove('hidden')
    }

    function hide(item) {
        item.classList.add('hidden')
    }

    function preventScroll(item) {
        item.classList.add('noscroll')
    }

    function allowScroll(item) {
        item.classList.remove('noscroll')
    }
}

document.addEventListener('DOMContentLoaded', modal);