$('.main-catalogue-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:true,
    dotsContainer: '.main-catalogue-slider-pages',
    navContainer: '.main-catalogue-slider-nav',
    navText: ['', ''],
    autoplay: true,
    autoplayTimeout: 5000,
    autoplaySpeed: 1000,
    responsive:{
        0:{
            items:2
        },
        950:{
            items:3
        },
        1200:{
            items:4
        }
    }
})

$('.promo-slider').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:true,
    dotsContainer: '.promo-slider-pages',
    navContainer: '.promo-slider-nav',
    navText: ['', ''],
    autoplay: true,
    autoplayTimeout: 5000,
    autoplaySpeed: 1000,
    responsive:{
        0:{
            items:2
        },
        950:{
            items:3
        },
        1200:{
            items:4
        }
    }
})
