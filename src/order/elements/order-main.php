<div class="content">
    <main class="main">
        <section class="cart">
            <h1 class="section--heading">Оформление заказа</h1>
            <div class="order-form--wrapper">
                <?php include($_SERVER['DOCUMENT_ROOT'].'/order/elements/form.html') ?>
            </div>
        </section>
    </main>
</div>