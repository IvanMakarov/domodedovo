<main class="main">
    <div class="content">
        <div class="breadcrumbs--wrapper">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        </div>   
        <div class="content--row__flex columns promo-item--wrapper">
            <article class="content--section promo-item ">
                <section class="promo-item--content columns">
                    <section class="promo-item--img">
                        <div class="promo-item--img-wrapper">
                            <img src="/img/promo/discount.jpg" alt="">
                        </div>
                    </section>
                    <section class="promo-item--details">
                        <div class="promo-item--details-wrapper">
                            <div class="content--row__flex">
                                <h1 class="promo-item--heading">
                                    Оптовые цены для всех при заказе от&nbsp;100&nbsp;000&nbsp;рублей!
                                </h1>
                                <section class="promo-item--date">
                                    до&nbsp;16.10.2017
                                </section>
                            </div>
                            <section class="promo-item--text">
                                <p>
                                    Сваи предназначены для применения во&nbsp;всех климатических районах, для устройства свайных фундаментов зданий и&nbsp;сооружений. Сваи предназначены для применения во&nbsp;всех климатических районах.
                                </p>
                                <p>
                                    Для устройства свайных фундаментов зданий и&nbsp;сооружений.Сваи предназначены для применения во&nbsp;всех климатических районах, для устройства свайных фундаментов зданий и&nbsp;сооружений<span class=""></span>
                                </p>
                            </section>
                        </div>
                    </section>
                </section>
            </article>
        </div>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
    </div>
</main>
