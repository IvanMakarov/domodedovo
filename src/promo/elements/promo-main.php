<main class="main">
    <div class="content">
        <div class="breadcrumbs--wrapper">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        </div>   
        <div class="content--row__flex columns promotion--wrapper">
            <section class="content--section promotion">
                <h2 class="section--heading">Акции</h2>
                <ul class="promotion--items columns">
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                    <li><?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-item-preview.html') ?></li>
                </ul>
            </section>
        </div>
    </div>
</main>
