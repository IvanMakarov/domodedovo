<div class="content">
    <main class="main">
        <div class="breadcrumbs--wrapper">
            <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/breadcrumbs.php') ?>
        </div>
        <div class="search-result--wrapper columns">
            <section class="search-result">
            <div class="content--row__flex search-result--heading-wrapper">
                <h1 class="section--heading">
                    Результаты поиска        
                </h1>
                <section class="search-result--counter">
                    <span>Найдено товаров:&nbsp;</span><span class="search-result--counter-value">3</span>    
                </section> 
            </div>
            <ul>
                <li>
                    <?php
                        $headerLevel = 'h2'; 
                        include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php');
                    ?>            
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php'); ?>
                </li>
                <li>
                    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/catalogue-item.php'); ?>
                </li>             
            </ul>
            </section>
        </div>
    </main>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/elements/promo-slider.php') ?>
</div>